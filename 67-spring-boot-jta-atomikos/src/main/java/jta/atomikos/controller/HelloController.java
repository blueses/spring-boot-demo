package jta.atomikos.controller;

import jta.atomikos.model.one.One;
import jta.atomikos.model.two.Two;
import jta.atomikos.one.service.OneService;
import jta.atomikos.service.HelloService;
import jta.atomikos.two.service.TwoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guo
 */
@RestController
public class HelloController {

    @Autowired
    private OneService oneService;

    @Autowired
    private TwoService twoService;

    @Autowired
    private HelloService helloService;

    @GetMapping("one/{id}")
    public Object getOne(@PathVariable String id){
        return oneService.selectById(id);
    }

    @GetMapping("two/{id}")
    public Object getTwo(@PathVariable String id){
        return twoService.selectById(id);
    }


    @PostMapping("insert/{oneId}/{oneOne}/{twoId}/{twoTwo}")
    public int insert(@PathVariable String oneId, @PathVariable String oneOne, @PathVariable String twoId, @PathVariable String twoTwo){
        One one = new One();
        one.setId(oneId);
        one.setOne(oneOne);
        Two two = new Two();
        two.setId(twoId);
        two.setTwo(twoTwo);
        return helloService.insert(one,two);
    }
}
