package jta.atomikos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author guo
 */
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class JtaAtomikosApplication {

	public static void main(String[] args) {
		SpringApplication.run(JtaAtomikosApplication.class, args);
	}

}

