package jdbc.service;

import jdbc.dao.UserDao;
import jdbc.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public User selectById(String id) {
        return userDao.selectById(id);
    }

    public List<User> selectList() {
        return userDao.selectList();
    }

    public int insert(User user) {
        return userDao.insert(user);
    }

    public int updateById(User user) {
        return userDao.updateById(user);
    }

    public int deleteById(String id) {
        return userDao.deleteById(id);
    }

}
