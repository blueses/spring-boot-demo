package spring.boot.mybatis;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author guo
 */
@Service
@Mapper
public interface SysUserMapper {

    public Integer insert(SysUser sysUser);

    public List<SysUser> findList();

}
