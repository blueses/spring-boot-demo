package jersey.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;


/**
 * @author guo
 */
@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        packages("jersey.controller");
    }

}
