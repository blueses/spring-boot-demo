package fast.controller;

import fast.service.FastService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@Controller
public class FastController {

    @Value("${fdfs.http.path}")
    public String path;

    @Autowired
    private FastService fastService;

    @GetMapping("/to/upload")
    public String picUpload() {
        return "upload";
    }

    @PostMapping("/upload")
    @ResponseBody
    public Map upload(MultipartFile file){
        String filename = fastService.uploadFile(file);
        Map<String, Object> data = new HashMap<>(16);
        data.put("original", file.getOriginalFilename());
        data.put("fileName",filename);
        data.put("httpUrl", path+filename);
        data.put("title", "图片上传");
        data.put("state", "SUCCESS");
        return data;
    }

    @PostMapping("/delete/pic")
    @ResponseBody
    public String deletePic(@RequestBody Map map){
        String url = (String) map.get("url");
        fastService.deleteFile(url);
        return "SUCCESS";
    }

}

