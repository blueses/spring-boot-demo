package spring.rabbitmq.onetomany;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "onetomany")
public class OneToManyMessageReceiver1 {

    @RabbitHandler
    public void receiver1(String message) {
        System.out.println("Receiver1  : " + message);
    }

}
