package spring.rabbitmq.onetoone;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "message")
public class OneToOneMessageReceiver {

    @RabbitHandler
    public void process(String message) {
        System.out.println("Receiver : " + message);
    }

}
