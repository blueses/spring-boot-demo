package provider.service;

import api.HelloService;
import com.alibaba.dubbo.config.annotation.Service;

import java.time.LocalDateTime;


/**
 * hello provider
 * @author blues
 */
@Service(version = "${spring.application.version}",
        application = "${dubbo.application.id}",
        protocol = "${dubbo.protocol.id}",
        registry = "${dubbo.registry.id}")
public class HelloServiceImpl implements HelloService {

    @Override
    public String hello(String name) {
        String msg = "Welcome " + name + " at " + LocalDateTime.now();
        System.out.println(msg);
        return msg;
    }
}