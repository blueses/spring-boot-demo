package consumer.controller;

import consumer.service.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Autowired
    private ConsumerService consumerService;

    @GetMapping("/hello")
    public String hello(){
        return consumerService.hello();
    }
}
