package many.datasource.mapper;

import many.datasource.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserMapper {

    @Insert("INSERT INTO user (id,name) values (#{id},#{name})")
    int insertUser(@Param("id") String id, @Param("name") String name);

    @Select("SELECT id,name FROM user")
    List<User> selectUserList();
}
