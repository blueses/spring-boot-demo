package many.datasource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author guo
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@MapperScan("many.datasource.mapper")
public class ManyDatasourcesApplication {

	public static void main(String[] args) {

		SpringApplication.run(ManyDatasourcesApplication.class, args);
	}
}
