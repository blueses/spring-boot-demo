package many.datasource.controller;

import many.datasource.entity.User;
import many.datasource.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/insert/{id}/{name}")
    public int insertUser(@PathVariable String id, @PathVariable String name){
        User user = new User();
        user.setId(id);
        user.setName(name);
        return userService.insertUser(user);
    }

    @GetMapping("/selectUser")
    public List<User> selectUser(){
        return userService.selectUser();
    }

}
