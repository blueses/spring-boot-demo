package spring.boot.async.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import spring.boot.async.service.HelloService;

@RestController
public class HelloController {
    private static final Log logger = LogFactory.getLog(HelloController.class);

    @Autowired
    private HelloService helloService;

    @GetMapping("hello")
    public String hello(){
        helloService.out();
        logger.info("hello controller hello world");
        return "hello";
    }
}



